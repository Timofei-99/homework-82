const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const router = express.Router();

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        if (req.query.artist) {
            const artistAlbums = await Album.find({artist: req.query.artist}).populate('artist', 'name');
            return res.send(artistAlbums);
        } else {
            const albums = await Album.find().populate('artist', 'name');
            res.send(albums);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findById(req.params.id).populate('artist', 'name description');

        if (album) {
            res.send(album)
        } else {
            res.status(404).send('Album not found');
        }
    } catch (e) {
        res.status(500).send(e);
    }
})

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.name || !req.body.year || !req.body.artist) {
        res.status(400).send('Not valid data');
    }

    const newAlbum = {
        name: req.body.name,
        year: req.body.year,
        artist: req.body.artist
    };

    if (req.file) {
        newAlbum.image = req.file.filename;
    }

    const album = new Album(newAlbum);

    try {
        await album.save();
        res.send(album);
    } catch (e) {
        res.status(400).send(e);
    }
})


module.exports = router;