const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const router = express.Router();


const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (e) {
        res.status(404).send(e);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.name) {
        res.status(400).send('Not valid data');
    }

    const artistsData = {
        name: req.body.name,
        description: req.body.description || null
    };

    if (req.file) {
        artistsData.image = req.file.filename;
    }

    const artist = new Artist(artistsData);

    try {
        await artist.save();
        res.send(artist);
    } catch (e) {
        res.status(400).send(e);
    }
});


module.exports = router;