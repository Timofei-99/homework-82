const express = require('express');
const Track = require('../models/Track');

const router = express.Router();


router.get('/', async (req, res) => {
    try {
        if (req.query.album) {
            const tracksInAlbum = await Track.find({album: req.query.album}).populate('album', 'name');
            res.send(tracksInAlbum);
        } else {
            const tracks = await Track.find();
            res.send(tracks);
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/', async (req, res) => {
    if (!req.body.name || !req.body.duration || !req.body.album) {
        res.status(400).send('Not valid data');
    }

    const track = {
        name: req.body.name,
        duration: req.body.duration,
        album: req.body.album
    }

    const tracks = new Track(track);

    try {
        await tracks.save()
        res.send(tracks);
    } catch (e) {
        res.status(400).send(e);
    }
});


module.exports = router;